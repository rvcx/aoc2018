import re
from collections import deque
from random import randint

parser = re.compile(r'pos=<(-?\d+),(-?\d+),(-?\d+)>, r=(\d+)')

bots = []

with open('input.txt') as f:
# with open('testdata.txt') as f:
  for l in f:
    x, y, z, r = map(int, parser.match(l).groups())
    bots.append((r, x, y, z))

minx = min(b[1] for b in bots)
miny = min(b[2] for b in bots)
minz = min(b[3] for b in bots)
maxx = max(b[1] for b in bots)
maxy = max(b[1] for b in bots)
maxz = max(b[1] for b in bots)

def inrange(x, y, z):
  out = 0
  for i in range(len(bots)):
    d = (abs(x - bots[i][1]) +
         abs(y - bots[i][2]) +
         abs(z - bots[i][3]))
    # print(bots[i], d)
    if d <= bots[i][0]:
      out += 1
  return out

def explore(x, y, z):
  best = inrange(x, y, z)
  print(best, x, y, z, x + y + z)
  for i in (1000000, 10000, 100, 1):
    changed = True
    while changed:
      changed = False
      for nx in (x-i, x, x+i):
        for ny in (y-i, y, y+i):
          for nz in (z-i, z, z+i):
            d = inrange(nx, ny, nz)
            if (d > best or d == best and (abs(nx) < abs(x) or
                                           abs(ny) < abs(y) or
                                           abs(nz) < abs(z))):
              x, y, z = nx, ny, nz
              best = d
              changed = True
  return best, x, y, z

best = 0
while True:
  xi, yi, zi = randint(minx, maxx), randint(miny, maxy), randint(minz, maxz)
  i = inrange(xi, yi, zi)
  d, x, y, z = explore(xi, yi, zi)
  if d > best:
    best = d
    print(d, x, y, z, x + y + z)
