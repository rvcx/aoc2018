import re

parser = re.compile(r'pos=<(-?\d+),(-?\d+),(-?\d+)>, r=(\d+)')

bots = []

with open('input.txt') as f:
# with open('testdata.txt') as f:
  for l in f:
    x, y, z, r = map(int, parser.match(l).groups())
    bots.append((r, x, y, z))

bots = list(reversed(sorted(bots)))
print(bots[0][0])
out = 0
for i in range(len(bots)):
  d = (abs(bots[0][1] - bots[i][1]) +
       abs(bots[0][2] - bots[i][2]) +
       abs(bots[0][3] - bots[i][3]))
  # print(bots[i], d)
  if d <= bots[0][0]:
    out += 1

print(out)