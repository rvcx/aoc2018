import re
from collections import defaultdict

parser = re.compile(r'()()')

xe, ye = 50, 50

def neighbors(c):
  x, y = c
  for nx in (x - 1, x, x + 1):
    for ny in (y - 1, y, y + 1):
      if (nx, ny) != c and 0 <= nx < xe and 0 <= ny < ye:
        yield (nx, ny)

with open('input.txt') as f:
  d = f.readlines()

# d = [
# '.#.#...|#.',
# '.....#|##|',
# '.|..|...#.',
# '..|#.....#',
# '#.#|||#|#|',
# '...#.||...',
# '.|....|...',
# '||...#|.#|',
# '|.||||..|.',
# '...#.|..|.'
# ]
# xe, ye = 10, 10

def n(cur, x, y):
  if cur == '.':
    count = 0
    for n in neighbors((x, y)):
      if d[n[1]][n[0]] == '|':
        count += 1
    if count >= 3:
      return '|'
    return '.'
  if cur == '|':
    count = 0
    for n in neighbors((x, y)):
      if d[n[1]][n[0]] == '#':
        count += 1
    if count >= 3:
      return '#'
    return '|'
  if cur == '#':
    counta = 0
    countb = 0
    for n in neighbors((x, y)):
      if d[n[1]][n[0]] == '#':
        counta += 1
      if d[n[1]][n[0]] == '|':
        countb += 1
    if counta and countb:
      return '#'
    return '.'

seen  = {}
for i in range(520):
  if i > 487:
    woods = sum(map(lambda s: s.count('|'), d))
    lumber = sum(map(lambda s: s.count('#'), d))
    print(i % 28, woods * lumber)
  d = tuple(''.join(n(d[y][x], x, y) for x in range(xe)) for y in range(ye))
  if d in seen:
    print('{} repeated at {}'.format(seen[d], i))
  seen[d] = i

woods = sum(map(lambda s: s.count('|'), d))
lumber = sum(map(lambda s: s.count('#'), d))
print(woods * lumber)