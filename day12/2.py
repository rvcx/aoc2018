import re
from collections import defaultdict
import sys

parser = re.compile(r'(.....) => (.)')

state = '##.#..########..##..#..##.....##..###.####.###.##.###...###.##..#.##...#.#.#...###..###.###.#.#'
# state = '#..#.#..##......###...###'
offset = 0

d = defaultdict(lambda: '.')
with open('input.txt') as f:
  for l in f:
    m = parser.match(l)
    if m:
      prev, n = m.groups()
      d[prev] = n

  for i in range(20000000):
    first = state.find('#')
    last = state.rfind('#')
    state = '....' + state[first:last + 1] + '....'
    offset += first - 2
    if not i %     5000000:
      print(i, state.count('#'), len(state), offset)
    state = ''.join(d[state[i-2:i+3]] for i in range(2, len(state) - 2))
    out = 0
    # print(offset, state)
    for j in range(len(state)):
      if state[j] == '#':
        # print(i + offset)
        out += j + offset
    print(i, out)
    if not state.count('#'):
      print('all died', i)
      sys.exit(0)
  # out = 0
  # print(state)
  # for i in range(len(state)):
  #   if state[i] == '#':
  #     print(i + offset)
  #     out += i + offset
  # print(out)
      