import re
from collections import defaultdict

circ = [0]
m = 1
mqxm = 71730
curi = 0
p = 0
np = 464
scores = defaultdict(int)

for m in range(1, 71731):
  if m % 23:
    curi = (curi + 2) % len(circ)
    circ.insert(curi, m)
  else:
    scores[p] += m
    curi = (curi - 7) % len(circ)
    scores[p] += circ[curi]
    del circ[curi:curi+1]
    curi = curi % len(circ)
  p = (p + 1) % np

print(max(scores.values()))