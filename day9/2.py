import re
from collections import defaultdict

marbles = [ [0, 0] ]

curi = 0
p = 0
np = 464
scores = [0] * np

for m in range(1, 7173001):
  if not m % 100000:
    print(m)
  if m % 23:
    n = marbles[curi]
    n = n[1]
    marbles.append([n, marbles[n][1]])
    marbles[marbles[n][1]][0] = m
    marbles[n][1] = m
    curi = m
  else:
    scores[p] += m
    marbles.append([])
    for _ in range(7):
      curi = marbles[curi][0]
    scores[p] += curi
    marbles[marbles[curi][0]][1] = marbles[curi][1]
    marbles[marbles[curi][1]][0] = marbles[curi][0]
    curi = marbles[curi][1]
  p = (p + 1) % np

print(max(scores))