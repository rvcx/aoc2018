import re
from collections import defaultdict
from collections import deque
import sys

parser = re.compile(r'()')

with open('input.txt') as f:
# with open('testdata.txt') as f:
  m = f.readlines()

def neighbors(c):
  y, x = c
  for n in ((y - 1, x), (y, x - 1), (y, x + 1), (y + 1, x)):
    if (0 <= n[0] < len(m) and 0 <= n[1] < len(m[n[0]])
        and m[n[0]][n[1]] != '#'):
      yield n

# p = defaultdict(lambda: 9999999)
# for y in range(len(m)):
#   for x in range(len(m[y])):
#     if m[y][x] != '#':
#       p[(y, x), (y, x)] = 0
#       for n in neighbors((y, x)):
#         p[((y, x), n)] = 1
#         p[(n, (y, x))] = 1
# for y in range(len(m)):
#   for x in range(len(m[y])):
#     if m[y][x] != '#':
#       for y2 in range(len(m)):
#         for x2 in range(len(m[y2])):
#           if m[y2][x2] != '#':
#             for y3 in range(len(m)):
#               for x3 in range(len(m[y3])):
#                 if m[y3][x3] != '#':
#                   newd = p[((y2, x2), (y, x))] + p[((y, x), (y3, x3))]
#                   if p[((y2, x2), (y3, x3))] > newd:
#                     p[((y2, x2), (y3, x3))] = newd

# print([v for v in p.values()])
goblins, elves = {}, {}
for y in range(len(m)):
  for x in range(len(m[y])):
    if m[y][x] == 'G':
      goblins[(y, x)] = 200
    elif m[y][x] == 'E':
      elves[(y, x)] = 200



def targets(enemies):
  for e in enemies.keys():
    for n in neighbors(e):
      yield n

def step(c, targets):
  targets = [t for t in targets]
  # print(c, targets)
  q, seen = deque((n, 1, n) for n in neighbors(c) if n not in goblins and n not in elves), set(n for n in neighbors(c))
  while q:
    dist = q[0][1]
    candidates = []
    while q and q[0][1] == dist:
      candidates.append(q.popleft())
    found = list(filter(lambda w: w[0] in targets, candidates))
    if found:
      # print(found)
      out = min(found)
      # print('{} moving towards {} via {}'.format(c, out[0], out[2]))
      return out[2]
    for (cur, dist, firststep) in candidates:
      for n in neighbors(cur):
        if n not in goblins and n not in elves and n not in seen:
          seen.add(n)
          q.append((n, dist + 1, firststep))
  return None
      
    
  
def moveThing(c, friends, enemies):
  minHP = 999999
  toHit = None
  for n in neighbors(c):
    if n in enemies and enemies[n] < minHP:
      toHit = n
      minHP = enemies[n]
  if toHit:
    print('{} hit {} (without moving)'.format(c, toHit))
    enemies[toHit] -= 3
    if enemies[toHit] <= 0:
      del enemies[toHit]
    return
  
  # # for t in targets(enemies):
  # #   print((c, t), (c, t) in p, p[(c, t)])
  # target = min((p[(c, t)], t) for t in targets(enemies))
  # # print('{} targetting {}'.format(c, target))
  # for n in neighbors(c):
  #   if p[(n, target[1])] == target[0] - 1:
  #     # print(friends, c)
  #     friends[n] = friends[c]
  #     del friends[c]
  #     break
  nextpos = step(c, targets(enemies))
  if not nextpos:
    return
  print('{} moved to {}'.format(c, nextpos))
  friends[nextpos] = friends[c]
  del friends[c]
  
  minHP = 999999
  toHit = None
  for n in neighbors(nextpos):
    if n in enemies and enemies[n] < minHP:
      toHit = n
      minHP = enemies[n]
  if toHit:
    print('{} hit {} (after moving)'.format(nextpos, toHit))
    enemies[toHit] -= 3
    if enemies[toHit] <= 0:
      del enemies[toHit]
    return

for round in range(500000000):
  print(round, elves, goblins)
  for c in sorted([i for i in goblins.keys()] + [i for i in elves.keys()]):
    # print(c)
    if c in goblins:
      moveThing(c, goblins, elves)
    elif c in elves:
      moveThing(c, elves, goblins)
    if not elves:
      print(round * sum(goblins.values()))
      sys.exit(0)
    if not goblins:
      print(round, sum(elves.values()))
      sys.exit(0)

