




def computenext(p):
  p |= 65536
  out = 12670166
  while True:
    # print(p, out)
    out += (p & 255)
    out &= 16777215
    out *= 65899
    out &= 16777215
    if p < 256:
      return out
    p = (p)//256
  
  
seen = {}
last = None  
x = 0
for i in range(9999999999):
  x = computenext(x)
  # print(i, x)
  if x in seen:
    print('done: ', i, x, seen[x], last, seen[last])
    break
  last = x
  seen[x] = i
