import re

parser = re.compile(r'#(\d+) @ (\d+),(\d+): (\d+)x(\d+)')

taken, mult = set(), set()
with open('input') as f:
  for l in f:
    i, x, y, w, h = map(int, parser.match(l).groups())
    for xp in range(x, x + w):
      for yp in range(y, y + h):
        if (xp, yp) in taken:
          mult.add((xp, yp))
        taken.add((xp, yp))
print(len(taken), len(mult))