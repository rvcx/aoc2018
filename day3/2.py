import re
from collections import defaultdict

parser = re.compile(r'#(\d+) @ (\d+),(\d+): (\d+)x(\d+)')

taken = defaultdict(set)
candidates = set()
with open('input') as f:
  for l in f:
    i, x, y, w, h = map(int, parser.match(l).groups())
    iscand = True
    for xp in range(x, x + w):
      for yp in range(y, y + h):
        print(taken[(xp, yp)])
        if taken[(xp, yp)]:
          iscand = False
        candidates -= taken[(xp, yp)]
        taken[(xp, yp)].add(i)
    if iscand:
      candidates.add(i)
print(candidates)