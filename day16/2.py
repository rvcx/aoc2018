import re
from collections import defaultdict

parseb = re.compile(r'Before: \[(\d+), (\d+), (\d+), (\d+)\]')
parsei = re.compile(r'(\d+) (\d+) (\d+) (\d+)')
parsea = re.compile(r'After:  \[(\d+), (\d+), (\d+), (\d+)\]')

def change(rs, r, v):
  out = list(rs)
  out[r] = v
  return out

impls = {
  'addr': lambda a, b, c, rs: change(rs, c, rs[a] + rs[b]),
  'addi': lambda a, b, c, rs: change(rs, c, rs[a] + b),
  'mulr': lambda a, b, c, rs: change(rs, c, rs[a] * rs[b]),
  'muli': lambda a, b, c, rs: change(rs, c, rs[a] * b),
  'banr': lambda a, b, c, rs: change(rs, c, rs[a] & rs[b]),
  'bani': lambda a, b, c, rs: change(rs, c, rs[a] & b),
  'borr': lambda a, b, c, rs: change(rs, c, rs[a] | rs[b]),
  'bori': lambda a, b, c, rs: change(rs, c, rs[a] | b),
  'setr': lambda a, b, c, rs: change(rs, c, rs[a]),
  'seti': lambda a, b, c, rs: change(rs, c, a),
  'gtir': lambda a, b, c, rs: change(rs, c, 1 if a > rs[b] else 0),
  'gtri': lambda a, b, c, rs: change(rs, c, 1 if rs[a] > b else 0),
  'gtrr': lambda a, b, c, rs: change(rs, c, 1 if rs[a] > rs[b] else 0),
  'eqir': lambda a, b, c, rs: change(rs, c, 1 if a == rs[b] else 0),
  'eqri': lambda a, b, c, rs: change(rs, c, 1 if rs[a] == b else 0),
  'eqrr': lambda a, b, c, rs: change(rs, c, 1 if rs[a] == rs[b] else 0),
}


imap = { i : set(impls.keys()) for i in range(16) }

with open('input.txt') as f:
  m = parseb.match(f.readline())
  while m:
    before = list(map(int, m.groups()))
    inst = list(map(int, parsei.match(f.readline()).groups()))
    after = list(map(int, parsea.match(f.readline()).groups()))
    
    for k, im in impls.items():
      good = False
      if im(inst[1], inst[2], inst[3], before) != after:
        print('removing', k, inst[0])
        imap[inst[0]].discard(k)
    f.readline()
    m = parseb.match(f.readline())

  while any(map(lambda v: len(v) > 1, imap.values())):
    for k, v in imap.items():
      if len(v) == 1:
        for k2, v2 in imap.items():
          if k != k2:
            v2.discard(list(v)[0])

  imap = { i : list(v)[0] for i, v in imap.items() }
  print(imap)
  print(f.readline())
  rs = [0, 0, 0, 0]
  while f:
    l = f.readline()
    # print('read', l)
    m = parsei.match(l)
    if not m:
      break
    i, a, b, c = map(int, m.groups())
    rs = impls[imap[i]](a, b, c, rs)

  print(rs)