import re
from collections import defaultdict

parseb = re.compile(r'Before: \[(\d+), (\d+), (\d+), (\d+)\]')
parsei = re.compile(r'(\d+) (\d+) (\d+) (\d+)')
parsea = re.compile(r'After:  \[(\d+), (\d+), (\d+), (\d+)\]')

def change(rs, r, v):
  out = list(rs)
  out[r] = v
  return out

impls = {
  'addr': lambda a, b, c, rs: change(rs, c, rs[a] + rs[b]),
  'addi': lambda a, b, c, rs: change(rs, c, rs[a] + b),
  'mulr': lambda a, b, c, rs: change(rs, c, rs[a] * rs[b]),
  'muli': lambda a, b, c, rs: change(rs, c, rs[a] * b),
  'banr': lambda a, b, c, rs: change(rs, c, rs[a] & rs[b]),
  'bani': lambda a, b, c, rs: change(rs, c, rs[a] & b),
  'borr': lambda a, b, c, rs: change(rs, c, rs[a] | rs[b]),
  'bori': lambda a, b, c, rs: change(rs, c, rs[a] | b),
  'setr': lambda a, b, c, rs: change(rs, c, rs[a]),
  'seti': lambda a, b, c, rs: change(rs, c, a),
  'gtir': lambda a, b, c, rs: change(rs, c, 1 if a > rs[b] else 0),
  'gtri': lambda a, b, c, rs: change(rs, c, 1 if rs[a] > b else 0),
  'gtrr': lambda a, b, c, rs: change(rs, c, 1 if rs[a] > rs[b] else 0),
  'eqir': lambda a, b, c, rs: change(rs, c, 1 if a == rs[b] else 0),
  'eqri': lambda a, b, c, rs: change(rs, c, 1 if rs[a] == b else 0),
  'eqrr': lambda a, b, c, rs: change(rs, c, 1 if rs[a] == rs[b] else 0),
}


count = 0
with open('input.txt') as f:
  m = parseb.match(f.readline())
  while m:
    before = list(map(int, m.groups()))
    inst = list(map(int, parsei.match(f.readline()).groups()))
    after = list(map(int, parsea.match(f.readline()).groups()))
    
    num = 0
    print('trying', inst, before)
    for k, im in impls.items():
      try:
        res = im(inst[1], inst[2], inst[3], before)
        print(k, res)
        if res == after:
          num += 1
      except:
        print(k, 'fault')
        pass
    print(num)
    if num >= 3:
      count += 1
    f.readline()
    m = parseb.match(f.readline())

print(count)
