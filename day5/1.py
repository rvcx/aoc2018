import re

alphabet = 'abcdefghijklmnopqrstuvwxyz'
exp = re.compile('|'.join('(' + c + c.upper() + ')|(' + c.upper() + c + ')' for c in alphabet))

with open('input.txt') as f:
  s = f.read().strip()
n = 1
while n:
  s, n = exp.subn('', s)

print(len(s))
