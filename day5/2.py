import re

alphabet = 'abcdefghijklmnopqrstuvwxyz'
exp = re.compile('|'.join('(' + c + c.upper() + ')|(' + c.upper() + c + ')' for c in alphabet))

with open('input.txt') as f:
  orig = f.read().strip()
for c in alphabet:
  s = orig.replace(c, '').replace(c.upper(), '')
  n = 1
  while n:
    s, n = exp.subn('', s)
  print(len(s))
