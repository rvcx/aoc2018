import re
from collections import defaultdict, deque
import types

parser = re.compile(r'(\d+) units each with (\d+) hit points (\(.*\) )?with an attack that does (\d+) (.*) damage at initiative (\d+)')

groups = []
# with open('testdata.txt') as f:
with open('input.txt') as f:
  f.readline()
  for side in ('immune', 'infect'):
    nextline = f.readline()[:-1]
    while True:
      l = ''
      while True:
        l += nextline
        nextline = f.readline()[:-1]
        if not nextline.startswith(' '):
          break
      m = parser.match(l)
      if not m:
        break
      units, hp, vuln, astr, atyp, initiative = m.groups()
      units, hp, astr, initiative = int(units), int(hp), int(astr), int(initiative)
      if not vuln:
        immunities, weaknesses = [], []
      else:
        immunities = re.search(r'immune to ([^;)]*)', vuln)
        immunities = [] if not immunities else [s.strip() for s in immunities.group(1).split(',')]
        weaknesses = re.search(r'weak to ([^;)]*)', vuln)
        weaknesses = [] if not weaknesses else [s.strip() for s in weaknesses.group(1).split(',')]
      groups.append(types.SimpleNamespace(side=side, units=units, hp=hp,
                                          imm=immunities, weak=weaknesses,
                                          astr=astr, atyp=atyp, ini=initiative))

boost = 28
for g in groups:
  if g.side == 'immune':
    g.astr += boost

while True:
  attacks = []
  for a in sorted(groups, key=lambda g:(g.units * g.astr, g.ini), reverse=True):
    candidates = list(filter(lambda d: a.side != d.side and d.units > 0 and
                        a.atyp not in d.imm and
                        d not in map(lambda x: x[1], attacks),
                        groups))
    def dam(d):
      return (2 if a.atyp in d.weak else 1, d.units * d.astr, d.ini)
    if candidates:
      candidates = sorted(candidates, key=dam, reverse=True)
      attacks.append((a, candidates[0]))

  attacks = sorted(attacks, key=lambda x: x[0].ini, reverse=True)
  for (a, d) in attacks:
    if a.units > 0:
      dam = a.astr * a.units
      if a.atyp in d.weak:
        dam *= 2
      d.units -= dam // d.hp
  sides = set()
  for g in groups:
    print(g.side, g.units, g.hp)
    if g.units > 0:
      sides.add(g.side)
  print()
  if len(sides) < 2:
    break


out = 0
winner = None
for g in groups:
  if g.units > 0:
    out += g.units
    winner = g.side
print(winner, out)