import re

clay = set()

parser = re.compile(r'(x|y)=(\d+), (x|y)=(\d+)..(\d+)')
#with open('testdata.txt') as f:
with open('input.txt') as f:
  for l in f:
    o, c, ocheck, rb, re = parser.match(l).groups()
    c, rb, re = int(c), int(rb), int(re)
    if o == 'x':
      assert ocheck == 'y'
      for y in range(rb, re + 1):
        clay.add((c, y))
    else:
      assert o == 'y'
      assert ocheck == 'x'
      for x in range(rb, re + 1):
        clay.add((x, c))

ymin = min(c[1] for c in clay)
ymax = max(c[1] for c in clay)

filled, poured = set(clay), set()

def pour(x, y):
  assert (x, y) not in clay, (x, y)
  # assert (x, y) not in filled, (x, y)
  while y < ymax and (x, y + 1) not in filled | poured:
    poured.add((x, y))
    y += 1
  if y == ymax or (x, y) in poured | filled or (x, y + 1) in poured:
    poured.add((x, y))
    return
  assert (x, y + 1) in filled
  while True:
    left = x - 1
    while (left, y + 1) in filled and (left, y) not in clay:
      left -= 1
    right = x + 1
    while (right, y + 1) in filled and (right, y) not in clay:
      right += 1
    if (left, y) in clay and (right, y) in clay:
      for cur in range(left + 1, right):
        filled.add((cur, y))
      y -= 1
    else:
      break
  for cur in range(left + 1, right):
    poured.add((cur, y))
  if (left, y) not in clay:
    pour(left, y)
  if (right, y) not in clay:
    pour(right, y)

pour(500, ymin)

xmin = min(c[0] for c in filled | poured)
xmax = max(c[0] for c in filled | poured)
for y in range(ymin, ymax + 1):
  print(''.join('#' if (x, y) in clay else
                '~' if (x, y) in filled else
                '|' if (x, y) in poured else
                '.' for x in range(xmin - 5, xmax + 6)))


print(len(filled - clay))






