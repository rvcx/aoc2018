import re
from collections import defaultdict
parser = re.compile(r'Step (.) must be finished before step (.) can begin.')

ready = set()
unfulfilled = defaultdict(set)
for c in 'ABCDEFGHINJKLMNOQRSTUVWXYZ':
  print('{} [label="{}"];'.format(c, c))
with open('input.txt') as f:
  for l in f:
    req, cons = parser.match(l).groups()
    print('{} -> {} ;'.format(req, cons))
    unfulfilled[cons].add(req)
    ready.add(req)

out = []
ready.difference_update(unfulfilled.keys())
while unfulfilled:
  print(ready)
  step = min(ready)
  ready.remove(step)
  out.append(step)
  to_remove = set()
  for k, v in unfulfilled.items():
    v.discard(step)
    if not v:
      to_remove.add(k)
  ready |= to_remove
  for k in to_remove:
    del unfulfilled[k]

print ''.join(out + sorted(ready))