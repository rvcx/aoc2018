import re
from collections import defaultdict
parser = re.compile(r'Step (.) must be finished before step (.) can begin.')

ready = set()
unfulfilled = defaultdict(set)
with open('input.txt') as f:
  for l in f:
    req, cons = parser.match(l).groups()
    unfulfilled[cons].add(req)
    ready.add(req)

ready.difference_update(unfulfilled.keys())
working = []
t = 0
while ready or working:
  while ready and len(working) < 5:
    step = min(ready)
    ready.remove(step)
    working.append((t + 61 + ord(step) - ord('A'), step))
  def complete(step):
    print('completed {} at {}'.format(step, t))
    to_remove = set()
    for k, v in unfulfilled.items():
      v.discard(step)
      if not v:
        to_remove.add(k)
        global ready
    ready |= to_remove
    for k in to_remove:
      del unfulfilled[k]
  working.sort(reverse=True)
  t, step = working.pop()
  complete(step)
  while working and working[-1][0] == t:
    complete(working.pop()[1])

