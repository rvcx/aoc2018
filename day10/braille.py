
def render(xb, xe, yb, ye, p):
  return '\n'.join(''.join(
    chr(0x2800 + p(x, y) + 8*p(x+1, y)
        +  2*p(x, y+1) +  16*p(x+1, y+1)
        +  4*p(x, y+2) +  32*p(x+1, y+2)
        + 64*p(x, y+3) + 128*p(x+1, y+3))
  for x in range(xb, xe, 2)) for y in range(yb, ye, 4))

      