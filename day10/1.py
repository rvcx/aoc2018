import re
from collections import defaultdict

parser = re.compile(r'position=< ?(-?\d+), +(-?\d+)> velocity=< ?(-?\d+), +(-?\d+)>')

out = ''
def render(xb, xe, yb, ye, pred):
  return '\n'.join(
    ''.join(
      chr(0x2800   + pred(x, y) +   8 * pred(x + 1, y)
          +  2 * pred(x, y + 1) +  16 * pred(x + 1, y + 1)
          +  4 * pred(x, y + 2) +  32 * pred(x + 1, y + 2)
          + 64 * pred(x, y + 3) + 128 * pred(x + 1, y + 3))
      for x in range(xb, xe, 2))
    for y in range(yb, ye, 4))

points = []
with open('input.txt') as f:
  for l in f:
    m = parser.match(l)
    points.append(list(map(int, m.groups())))

minxd, minyd = 999999, 999999
for _ in range(100000):
  xmin, ymin = points[0][0], points[0][1]
  xmax, ymax = xmin, ymin
  for p in points:
    p[0] += p[2]
    p[1] += p[3]
    if p[0] < xmin:
      xmin = p[0]
    if p[0] > xmax:
      xmax = p[0]
    if p[1] < ymin:
      ymin = p[1]
    if p[1] > ymax:
      ymax = p[1]
  if xmax - xmin < 100:
    print(render(xmin, xmax + 1, ymin, ymax + 1,
                 lambda x, y: any(map(lambda p: p[0] == x and p[1] == y, points))))
    # for y in range(ymin, ymax+1):
    #   print(''.join('#' if any(map(lambda p: p[0] == x and p[1] == y, points))
    #                 else ' ' for x in range(xmin, xmax + 1)))
    print('----------------------------------------------')
  minxd = min(minxd, xmax - xmin)
  minyd = min(minxd, ymax - ymin)
print(minxd, minyd)