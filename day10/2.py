import re
from collections import defaultdict

parser = re.compile(r'position=< ?(-?\d+), +(-?\d+)> velocity=< ?(-?\d+), +(-?\d+)>')

out = ''

points = []
with open('input.txt') as f:
  for l in f:
    m = parser.match(l)
    points.append(map(int, m.groups()))

minxd, minyd = 999999, 999999
for t in range(100000):
  xmin, ymin = points[0][0], points[0][1]
  xmax, ymax = xmin, ymin
  for p in points:
    p[0] += p[2]
    p[1] += p[3]
    if p[0] < xmin:
      xmin = p[0]
    if p[0] > xmax:
      xmax = p[0]
    if p[1] < ymin:
      ymin = p[1]
    if p[1] > ymax:
      ymax = p[1]
  if xmax - xmin < 100:
    for y in range(ymin, ymax+1):
      print(''.join('#' if any(map(lambda p: p[0] == x and p[1] == y, points))
                    else ' ' for x in range(xmin, xmax + 1)))
    print(t, '----------------------------------------------')
  minxd = min(minxd, xmax - xmin)
  minyd = min(minxd, ymax - ymin)
print(minxd, minyd)