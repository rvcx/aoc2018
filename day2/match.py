with open('boxes') as f:
    ids = set()
    for line in f:
        ids.add(line)
for i in ids:
    for pos in range(len(i)):
        for letter in 'abcdefghijklmnopqrstuvwxyz':
            newid = i[:pos] + letter + i[pos+1:]
            if newid != i and newid in ids:
                print(i, newid)