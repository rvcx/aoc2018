import collections
with open('boxes') as f:
    twos, threes = 0, 0
    for line in f:
        d = collections.defaultdict(int)
        for c in line:
            d[c] += 1
        if 2 in d.values():
            twos += 1
        if 3 in d.values():
            threes += 1
    print(twos * threes)