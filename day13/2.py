import re
from collections import defaultdict
import sys

# with open('input.txt') as f:
with open('input.txt') as f:
  grid = f.readlines()

mx, my = max(len(row) for row in grid), len(grid)

carts = []
for x in range(mx):
  for y in range(my):
    if x < len(grid[y]):
      if grid[y][x] in '<>':
        carts.append([y, x, grid[y][x], 0])
        # grid[y][x] = '-'
      elif grid[y][x] in '^v':
        carts.append([y, x, grid[y][x], 0])
        # grid[y][x] = '|'

def cartat(y, x):
  for c in carts:
    if c[0] == y and c[1] == x:
      print('crash at x={} y={}'.format(x, y))
      c[2] = 'x'
      return True
  return False

for t in range(500000000000):
  print(t)
  for c in sorted(carts):
    if c[2] == 'x':
      next
    print(c)
    if c[2] == '>':
      ny, nx = c[0], c[1] + 1
      if not cartat(ny, nx):
        if grid[ny][nx] == '/':
          c[2] = '^'
        elif grid[ny][nx] == '\\':
          c[2] = 'v'
        elif grid[ny][nx] == '+':
          if c[3] == 0:
            c[2] = '^'
            c[3] = 1
          elif c[3] == 1:
            c[3] = 2
          elif c[3] == 2:
            c[2] = 'v'
            c[3] = 0
        else:
          assert grid[ny][nx] in '><-'
        c[0], c[1] = ny, nx
      else:
        c[2] = 'x'
    elif c[2] == '<':
      ny, nx = c[0], c[1] - 1
      if not cartat(ny, nx):
        if grid[ny][nx] == '/':
          c[2] = 'v'
        elif grid[ny][nx] == '\\':
          c[2] = '^'
        elif grid[ny][nx] == '+':
          if c[3] == 0:
            c[2] = 'v'
            c[3] = 1
          elif c[3] == 1:
            c[3] = 2
          elif c[3] == 2:
            c[2] = '^'
            c[3] = 0
        else:
          assert grid[ny][nx] in '<>-'
        c[0], c[1] = ny, nx
      else:
        c[2] = 'x'
    elif c[2] == '^':
      ny, nx = c[0] - 1, c[1]
      if not cartat(ny, nx):
        if grid[ny][nx] == '/':
          c[2] = '>'
        elif grid[ny][nx] == '\\':
          c[2] = '<'
        elif grid[ny][nx] == '+':
          if c[3] == 0:
            c[2] = '<'
            c[3] = 1
          elif c[3] == 1:
            c[3] = 2
          elif c[3] == 2:
            c[2] = '>'
            c[3] = 0
        else:
          assert grid[ny][nx] in '^v|'
        c[0], c[1] = ny, nx
      else:
        c[2] = 'x'
    elif c[2] == 'v':
      ny, nx = c[0] + 1, c[1]
      # print(ny, nx, grid[ny][nx])
      if not cartat(ny, nx):
        if grid[ny][nx] == '/':
          c[2] = '<'
        elif grid[ny][nx] == '\\':
          c[2] = '>'
        elif grid[ny][nx] == '+':
          # print('turning', c[3])
          if c[3] == 0:
            c[2] = '>'
            c[3] = 1
          elif c[3] == 1:
            c[3] = 2
          elif c[3] == 2:
            c[2] = '<'
            c[3] = 0
        else:
          assert grid[ny][nx] in '^v|'
        c[0], c[1] = ny, nx
      else:
        c[2] = 'x'
  carts = list(filter(lambda c: c[2] != 'x', carts))
  if len(carts) < 2:
    print(carts)
    break
out = ''

print(out)