import re
from collections import defaultdict, deque

# with open('testdata.txt') as f:
# # with open('input.txt') as f:
#   for l in f:
#     a, b = parser.match(l).groups()

s = '^WNE$'
s = '^ENWWW(NEEE|SSE(EE|N))$'
s = '^ENNWSWW(NEWS|)SSSEEN(WNSE|)EE(SWEN|)NNN$'
s = '^ESSWWN(E|NNENN(EESS(WNSE|)SSS|WWWSSSSE(SW|NNNE)))$'
s = '^WSSEESWWWNW(S|NENNEEEENN(ESSSSW(NWSW|SSEN)|WSWWN(E|WWS(E|SS))))$'
with open('input.txt') as f:
  s = f.read()
pos = 0
doors = set()

memo = {}
def follow(ix, iy, si):
  x, y, s = ix, iy, si
  if (x, y, s) in memo:
    return memo[(x, y, s)]
  dests = set()
  while True:
    # print(x, y, s)
    if s[0] in '$)|':
      dests.add((x, y))
      memo[(ix, iy, si)] = (dests, s)
      return (dests, s)
    elif s[0] == 'E':
      doors.add((x + 1, y))
      x += 2
    elif s[0] == 'W':
      doors.add((x - 1, y))
      x -= 2
    elif s[0] == 'S':
      doors.add((x, y + 1))
      y += 2
    elif s[0] == 'N':
      doors.add((x, y - 1))
      y -= 2
    elif s[0] == '(':
      nexts = set()
      while s[0] in '(|':
        starts, s = follow(x, y, s[1:])
        nexts |= starts
      assert s[0] == ')', s
      for x, y in nexts:
        nd, ns = follow(x, y, s[1:])
        dests |= nd
      return (dests, ns)
    s = s[1:]

follow(0, 0, s[1:])

for y in range(min(y for x, y in doors) - 1, max(y for x, y in doors) + 2):
  print(''.join('.' if not x % 2 and not y % 2 else
                '-' if (x, y) in doors and y % 2 else
                '|' if (x, y) in doors else
                '#'
                for x in range(min(x for x, y in doors) - 1,
                               max(x for x, y in doors) + 2)))

q = deque()
q.append(((0, 0), 0))
visited = {(0, 0) : 0}
while q:
  (x, y), p = q.popleft()
  for d, n in [((x + 1, y), (x + 2, y)),
               ((x - 1, y), (x - 2, y)),
               ((x, y + 1), (x, y + 2)),
               ((x, y - 1), (x, y - 2))]:
    if d in doors and n not in visited:
      visited[n] = p + 1
      q.append((n, p + 1))

print(max(visited.values()))