import re
from collections import defaultdict

parser = re.compile(r'()()')

with open('input.txt') as f:
  nums = [ i for i in reversed(map(int, f.read().split()))]

out = 0
def readn():
  global out
  children, metas = nums.pop(), nums.pop()
  for _ in range(children):
    readn()
  for _ in range(metas):
    out += nums.pop()

readn()

print(out)