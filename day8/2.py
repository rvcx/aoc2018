import re
from collections import defaultdict

parser = re.compile(r'()()')

with open('input.txt') as f:
  nums = [ i for i in reversed(map(int, f.read().split()))]

def readn():
  children, metas = nums.pop(), nums.pop()
  vals = [ readn() for _ in range(children) ]
  print(vals)
  ms = [ nums.pop() for _ in range(metas) ]
  print(ms)
  if not vals:
    print('ret', sum(ms))
    return sum(ms)
  tot = 0
  for m in ms:
    if m <= len(vals):
      tot += vals[m - 1]
  print('ret', tot)
  return tot

print(readn())