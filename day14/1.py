import re
from collections import defaultdict

n = 681901

recipes = [ 3, 7 ]

first, second = 0, 1

for _ in range(n + 20):
  newr = recipes[first] + recipes[second]
  if newr > 9:
    recipes.append(newr // 10)
  recipes.append(newr % 10)

  first = (first + 1 + recipes[first]) % len(recipes)
  second = (second + 1 + recipes[second]) % len(recipes)

print(recipes[n:n+10])