import re
from collections import deque, defaultdict

parser = re.compile(r'()()')

def neighbors(p):
  x, y, z, t = p
  for dx in range(4):
    for dy in range(4):
      for dz in range(4):
          for dt in range(4):
            if dx + dy + dz + dt <= 3:
              yield (x + dx, y + dy, z + dz, t + dt)
              yield (x + dx, y + dy, z + dz, t - dt)
              yield (x + dx, y + dy, z - dz, t + dt)
              yield (x + dx, y + dy, z - dz, t - dt)
              yield (x + dx, y - dy, z + dz, t + dt)
              yield (x + dx, y - dy, z + dz, t - dt)
              yield (x + dx, y - dy, z - dz, t + dt)
              yield (x + dx, y - dy, z - dz, t - dt)
              yield (x - dx, y + dy, z + dz, t + dt)
              yield (x - dx, y + dy, z + dz, t - dt)
              yield (x - dx, y + dy, z - dz, t + dt)
              yield (x - dx, y + dy, z - dz, t - dt)
              yield (x - dx, y - dy, z + dz, t + dt)
              yield (x - dx, y - dy, z + dz, t - dt)
              yield (x - dx, y - dy, z - dz, t + dt)
              yield (x - dx, y - dy, z - dz, t - dt)

ctop, ptoc = defaultdict(set), {}

# with open('testdata.txt') as f:
with open('input.txt') as f:
  for l in f:
    tomerge = set()
    p = tuple(map(int, l.split(',')))
    for n in neighbors(p):
      if n in ptoc:
        tomerge.add(ptoc[n])
    if not tomerge:
      ctop[p].add(p)
      ptoc[p] = p
    else:
      mainc = tomerge.pop()
      print('merging', p, mainc)
      ctop[mainc].add(p)
      ptoc[p] = mainc
      for c in tomerge:
        for p in ctop[c]:
          ptoc[p] = mainc
        ctop[mainc] |= ctop[c]
        del ctop[c]

print(len(ctop))
