import re
from collections import defaultdict, deque
import heapq

depth = 4845
tx, ty = 6, 770

# depth = 510
# tx, ty = 10, 10

el = { (0, 0) : 0 }
for x in range(tx * 10 + 1):
  gi = x * 16807
  el[(x, 0)] = ((gi + depth) % 20183)
  for y in range(1, ty * 2 + 1):
    gi = y * 48271 if not x else el[(x - 1, y)] * el[(x, y - 1)]
    el[(x, y)] = ((gi + depth) % 20183)
el[(tx, ty)] = 0

times = defaultdict(lambda: (999999999999, []))

def tools(x, y):
  return (('c', 't'), ('c', 'n'), ('t', 'n'))[el[(x, y)] % 3]

times[(0, 0, 't')] = (0, [])
q = [(0, 0, 0, 't', [])]
while q:
  time, xi, yi, ti, path = heapq.heappop(q)
  for x, y in ((xi - 1, yi), (xi, yi - 1), (xi + 1, yi), (xi, yi + 1)):
    if 0 <= x < tx * 10 and 0 <= y <= ty * 2:
      for t in tools(x, y):
        if t not in tools(xi, yi):
          continue
        nt = time + 1
        np = list(path)
        if t != ti:
          nt += 7
          np.append(t)
        np.append((x, y))
        print(x, y, t, nt)
        if times[(x, y, t)][0] > nt:
          times[(x, y, t)] = (nt, np)
          heapq.heappush(q, (nt, x, y, t, np))

print(times[(tx, ty, 't')][0])
print(times[(tx, ty, 'c')][0] + 7)
print(times[(tx, ty, 'n')][0] + 7)
