import re
from collections import defaultdict, deque

depth = 4845
tx, ty = 6, 770

# depth = 510
# tx, ty = 10, 10

el = { (0, 0) : 0, (tx, ty) : 0 }

risk = 0


for x in range(tx + 1):
  gi = x * 16807
  el[(x, 0)] = ((gi + depth) % 20183)
  risk += el[(x, 0)] % 3
  print(x, 0, risk)
  for y in range(1, ty + 1):
    gi = y * 48271 if not x else el[(x - 1, y)] * el[(x, y - 1)]
    el[(x, y)] = ((gi + depth) % 20183)
    risk += el[(x, y)] % 3
    print(x, y, risk)

print(risk)
