maxx = 313
maxy = 342

from collections import defaultdict

d = defaultdict(int)

with open('input.txt') as f:
  i = 0
  for l in f:
    i += 1
    xp, yp = map(int, l.split(','))
    for x in range(-100, maxx + 100):
      for y in range(-100, maxy + 100):
        dist = abs(x - xp) + abs(y - yp)
        d[(x, y)] += dist

out = 0
for x in range(-100, maxx + 100):
  for y in range(-100, maxy + 100):
    if d[(x, y)] < 10000:
      out += 1

print(out)
