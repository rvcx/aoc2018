maxx = 313
maxy = 342

d = {}

from collections import defaultdict

for x in range(maxx + 2):
  for y in range(maxy + 2):
    d[(x, y)] = (0, 999)

with open('input.txt') as f:
  i = 0
  for l in f:
    i += 1
    xp, yp = map(int, l.split(','))
    for x in range(maxx + 2):
      for y in range(maxy + 2):
        dist = abs(x - xp) + abs(y - yp)
        if dist < d[(x, y)][1]:
          d[(x, y)] = (i, dist)
        elif dist == d[x, y][1]:
          d[(x, y)] = (0, dist)

#for y in range(maxy + 2):
#  print(''.join(d[(x, y)][0]))

areas = defaultdict(int)
for x in range(maxx + 2):
  for y in range(maxy + 2):
    areas[d[(x, y)][0]] += 1
for x in range(maxx + 2):
  areas[d[(x, 0)][0]] = 0
  areas[d[(x, maxy + 1)][0]] = 0
for y in range(maxy + 2):
  areas[d[(0, y)][0]] = 0
  areas[d[(maxx + 1, y)][0]] = 0

print(max(areas.values()))