import re
from collections import defaultdict

parser = re.compile(r'\[(\d\d\d\d)-(\d\d)-(\d\d) (\d\d):(\d\d)\] (.*)')
guard_exp = re.compile(r'Guard #(\d+) begins shift')

entries = []
with open('input.txt') as f:
  for l in f:
    m = parser.match(l)
    when = tuple(map(int, m.groups()[:-1]))
    tail = m.groups()[-1]
    entries.append((when, tail))

def daymin(when):
  return when[-2] * 60 + when[-1]

entries = list(reversed(sorted(entries)))
slept = defaultdict(lambda: defaultdict(int))
while entries:
  when, tail = entries.pop()
  gid = int(guard_exp.match(tail).group(1))
  while entries and not guard_exp.match(entries[-1][1]):
    sleep, tail = entries.pop()
    assert tail.startswith('falls')
    wake, tail = entries.pop()
    assert tail.startswith('wakes')
    for m in range(daymin(sleep), daymin(wake)):
      slept[gid][m] += 1

i, mins = 0, 0
for g, s in slept.items():
  if max(s.values()) > mins:
    i = g
    mins = max(s.values())
worst = max(slept[i].values())
for m, amount in slept[i].items():
  if amount == worst:
    print(i * m)
