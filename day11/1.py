import re
from collections import defaultdict

serial = 4172

# serial = 8

def val(x, y):
  return (((x + 10) * y + serial) * (x + 10) // 100) % 10 - 5

bestv = -999999999
best = None
for c in range(1, 301):
  for x in range(1, 300 - c + 1):
    for y in range(1, 300 - c + 1):
      v = sum(val(x+i,y+j) for j in range(c) for i in range(c))
    # v = (val(i, j) + val(i + 1, j) + val(i + 2, j) +
    # val(i, j+1) + val(i + 1, j+1) + val(i + 2, j+1) +
    # val(i, j+2) + val(i + 1, j+2) + val(i + 2, j+2))
      if v > bestv:
        print(bestv, v, c, x, y)
        bestv = v
        best = (x, y, c)

print(best)