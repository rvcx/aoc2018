
def calc(x):
  out = 0
  for i in range(1, x + 1):
    if x % i == 0:
      out += i
  return out

print(calc(896))
print(calc(10551296))
