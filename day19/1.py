import re
from collections import defaultdict, deque

parser = re.compile(r'(.*) (\d+) (\d+) (\d+)')

def change(rs, r, v):
  out = list(rs)
  out[r] = v
  return out

impls = {
  'addr': lambda a, b, c, rs: change(rs, c, rs[a] + rs[b]),
  'addi': lambda a, b, c, rs: change(rs, c, rs[a] + b),
  'mulr': lambda a, b, c, rs: change(rs, c, rs[a] * rs[b]),
  'muli': lambda a, b, c, rs: change(rs, c, rs[a] * b),
  'banr': lambda a, b, c, rs: change(rs, c, rs[a] & rs[b]),
  'bani': lambda a, b, c, rs: change(rs, c, rs[a] & b),
  'borr': lambda a, b, c, rs: change(rs, c, rs[a] | rs[b]),
  'bori': lambda a, b, c, rs: change(rs, c, rs[a] | b),
  'setr': lambda a, b, c, rs: change(rs, c, rs[a]),
  'seti': lambda a, b, c, rs: change(rs, c, a),
  'gtir': lambda a, b, c, rs: change(rs, c, 1 if a > rs[b] else 0),
  'gtri': lambda a, b, c, rs: change(rs, c, 1 if rs[a] > b else 0),
  'gtrr': lambda a, b, c, rs: change(rs, c, 1 if rs[a] > rs[b] else 0),
  'eqir': lambda a, b, c, rs: change(rs, c, 1 if a == rs[b] else 0),
  'eqri': lambda a, b, c, rs: change(rs, c, 1 if rs[a] == b else 0),
  'eqrr': lambda a, b, c, rs: change(rs, c, 1 if rs[a] == rs[b] else 0),
}

program = []
# with open('testdata.txt') as f:
with open('input.txt') as f:
  f.readline()
  ipr = 1
  for l in f:
    i, a, b, c = parser.match(l).groups()
    a, b, c = int(a), int(b), int(c)
    program.append((i, a, b, c))

rs = [0, 0, 0, 0, 0, 0]
while rs[ipr] < len(program):
  i, a, b, c = program[rs[ipr]]
  rs = impls[i](a, b, c, rs)
  rs[ipr] += 1

print(rs)
