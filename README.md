# 2018 Advent of Code solutions

This repo contains my solutions for all 25 days of [the 2018 Advent of Code](https://adventofcode.com/2018). I haven't gone back to clean any of this up after getting the right answer, and (as I detail below) the race for time very much changed the way I program. What you see here is definitely not great engineering practice, and in many cases is pretty ugly Python, because that's what came out of my fingers first (or was expedient in the course of debugging).

## My experience

This was for the most part my first exposure to "competitive programming". I was technically on my high school's programming team (of two), but that was only because I was a captain of the math team and they wanted somebody clever to offer their thoughts to the one dude who actually knew how to program. I recall coming up with a bunch of approaches to those problems but being constantly surprised by which ones the programmer told me were easy to implement and which ones he said were hard.

Many weeks over the past year I blocked out twenty minutes of my Friday to see if a few lines of Python code could find a solution to that week's [Riddler, from 538](https://fivethirtyeight.com/tag/the-riddler/). This was mostly about me trying to wean myself off pure analytical solutions and embrace more modern computational approaches: quick and dirty monte carlo simulation can often get more than enough precision in a fraction of the time it would take me to derive a symbolic solution. It wasn't timed, and I wouldn't really call it a programming competition.

That said, I have been a professional software engineer (mostly C++) for something close to twenty years (albeit mainly just an architect and manager the past three) and I have a doctorate in algorithm design, so, like, I ought to be able to solve a few puzzles, right?

## On competitive programming code

The biggest shock to me in the first few days was looking at the leaderboard and seeing that people were solving problems in less than the time it took me just to read the puzzle descriptions. Further, I managed every one of these solutions in under a hundred lines of Python code, and the puzzle platform tells you when you get the right answer. Those are a completely different set of pressures than those informing the kind of production code I've built for most of my career. Knowing that there was no collaboration or maintenance involved, and that I could easily fit each problem into my head at once, made this new (to me) style of programming kind of fun. No worrying about pesky testing or modularization concerns: just focus on making the computer do what I want.

I don't know if doing these sorts of competitions makes you a better engineer. I'd certainly hope that anyone I worked with would be able to solve all of these problems (given a few hours)...but for the most part programming is the easiest part of software engineering. It's definitely good fun for a manager who doesn't get the chance to write as much code as he'd like, and writing something new from beginning to end every day lets you flex a lot of programming muscles that may not get much exercise even if you are regularly working on production code. Some book (maybe [The Pragmatic Programmer](https://en.wikipedia.org/wiki/The_Pragmatic_Programmer)?) once recommended that coders do regular "code katas", and this seems to qualify.

The one place I definitely recommend this kind of programming is if you're learning a new language. A new program from scratch every day (done at a pretty quick pace) will definitely help you internalize language syntax and basic library interfaces.

## On Advent Of Code itself

I'd never heard of this until a post I saw on the afternoon of 1 December, and fifteen minutes later I'd done the first (admittedly trivial) puzzle, so I stuck with it. Apparently over 75,000 different people have solved at least one puzzle, which is just astonishing; I have trouble getting my head around that many people writing code for fun.

I'm amazed by how much work [Eric Wastl](http://was.tl/) must have spent putting it all together. If I had to criticise the puzzles themselves I'd say that they were all a little straightforward---nothing terribly interesting algorithmically. But I actually rather like that the whole thing is so accessible. Anybody with the equivalent of even a semester or two of formal training should be able to work through all of these without much danger of getting "stuck" by some crux or piece of obscure math they couldn't overcome.

## Success and scoring

I thought of my success on each puzzle in four tiers:

1. Solved the puzzle at all.
2. Solved it on the day it was released (under 24 hours).
3. Solved it as soon as it came out at midnight, in one sitting.
4. Actually made the top 100 score table.

I only hit tier 4 for four of the puzzles, but actually stayed up and managed tier 3 for 2/3 of them. There was only one that I didn't get around to the day it was released (on day 4).

I'm sure with a bit more practice at this style of programming I'd be able to turn more tier 3 finishes into tier 4 (I often finished in the 100-200 range), but for me the time of day was a bit of a problem. Even if I can spare an hour (or more...day 15 took me two and a half hours), coding even a trivial programming puzzle rapidly really keyed me up and made it hard for me to get to sleep for an hour or two afterward. What's more, when I did wait until the following day to look at a puzzle I found I was much quicker, and my code much cleaner, in solving it; it's clear I'm just not as sharp in the middle of the night as I am in the daytime.

It's also a little disappointing that the official scoring only counts the top 100 when tens of thousands of people participate...and literally thousands are in fact doing them competitively at midnight. It seems like extending the scoring to top 500 or so would give official recognition to my tier 3 finishes, which would make me feel less silly for staying up so late.

That said, I think I enjoyed the puzzles more when I wasn't scrambling for minutes (and in fact on a couple of the later ones I rather intentionally dawdled doing a few tests and the like even though I knew the clock was running and it wasn't the optimal strategy). If I bother with it again next year I might try to talk myself out of any of the "competitive" aspect.

But if anyone reading this would like me to join their private leaderboard for 2019, I admit I'd consider it.

## Finally

This was an educational and enjoyable experience for me, so I [threw a few bucks](https://adventofcode.com/2018/support) towards supporting Advent of Code. I recommend others do so as well.